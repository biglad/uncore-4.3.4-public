ALTER TABLE `realmlist` ADD COLUMN `lan_address` VARCHAR(255) NOT NULL DEFAULT '127.0.0.1' AFTER address;

ALTER TABLE `realmlist`
    CHANGE `address` `wan_address` VARCHAR(255) NOT NULL DEFAULT '127.0.0.1';

DROP TABLE IF EXISTS `lan_access`;
CREATE TABLE `lan_access` (
    `ip` varchar(255) NOT NULL DEFAULT '127.0.0.1',
    PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;